# Creditstar Group Recruitment Exercise

Submission by Brian Matovu <mtvbrianking@gmail.com>

## Setup

**Update your vendor packages**

    docker-compose run --rm php composer update --prefer-dist

Run the installation triggers (creating cookie validation code)

    docker-compose run --rm php composer install    

**Set environment variables**

Add your database configurations to the `.env` file

    .env

**Start the container**

    docker-compose up -d

**Migrate database**

    docker-compose run --rm php yii migrate

**Seed database**

    docker-compose run --rm php yii fixture/load Staff
    docker-compose run --rm php yii fixture/load User
    docker-compose run --rm php yii fixture/load Loan

**Served at**

    http://127.0.0.1:8000

## Testing

**Migrate test database**

    docker-compose run --rm php tests/bin/yii migrate

**Seed test database**

    docker-compose run --rm php tests/bin/yii fixture/load Staff

**Run tests**

    docker-compose run --rm php codecept run
