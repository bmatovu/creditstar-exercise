<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property int $personal_code
 * @property int $phone
 * @property bool|null $active
 * @property bool|null $dead
 * @property string|null $lang
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'email', 'personal_code', 'phone'], 'required'],
            [['first_name', 'last_name', 'email', 'lang'], 'string'],
            [['personal_code', 'phone'], 'default', 'value' => null],
            [['personal_code', 'phone'], 'integer'],
            [['personal_code'], 'checkPersonalCode'],
            [['active', 'dead'], 'boolean'],
            [['personal_code'], 'unique', 'targetClass' => '\app\models\User', 'message' => 'Personal Code already taken.'],
            [['email'], 'unique', 'targetClass' => '\app\models\User', 'message' => 'Email address already taken.'],
        ];
    }

    /**
     * Get user's full name.
     *
     * @return string
     */
    public function getName()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    /**
     * Get person birth century.
     *
     * @return int Birth century
     */
    protected function getBirthCentury() : int
    {
        $genderNo = substr($this->personal_code, 0, 1);

        return 1700 + ceil($genderNo / 2) * 100;
    }

    /**
     * Get user's birthday.
     *
     * @return \Datetime
     */
    public function getBirthDate() : \Datetime
    {
        $year = $this->getBirthCentury() + substr($this->personal_code, 1, 2);
        $month = substr($this->personal_code, 3, 2);
        $day = substr($this->personal_code, 5, 2);

        $dob = "{$year}-{$month}-{$day}";

        return new \Datetime($dob);
    }

    /**
     * Get user's age.
     *
     * @param \Datetime $now
     *
     * @return int
     */
    public function getAge(\Datetime $now = null) : int
    {
        if (is_null($now)) {
            $now = new \Datetime();
        }

        return $now->diff($this->getBirthDate())->y;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'name' => Yii::t('app', 'Name'),
            'age' => Yii::t('app', 'Age'),
            'birthDate' => Yii::t('app', 'Date Of Birth'),
            'email' => 'Email Address',
            'personal_code' => 'Personal Code',
            'phone' => 'Phone Number',
            'active' => 'Is Active',
            'dead' => 'Is Dead',
            'lang' => 'Language',
        ];
    }

    /**
     * Loans this user tool.
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLoans()
    {
        return $this->hasMany(Loan::className(), [
            'user_id' => 'id',
        ]);
    }

    /**
     * Validate personal code input.
     */
    public function checkPersonalCode()
    {
        if (! $this->hasErrors() && ! $this->isValidPersonalCode($this->personal_code)) {
            $this->addError('personal_code', 'Invalid personal code.');
        }
    }

    /**
     * Validate personal code.
     *
     * @param string|null $personal_code
     *
     * @return boolean
     */
    protected function isValidPersonalCode($personal_code = null)
    {
        $personal_code = is_null($personal_code) ? $this->personal_code : $personal_code;

        return preg_match('/^[1-6][0-9]{2}[0-1][0-9][0-9]{2}[0-9]{4}$/', $personal_code);
    }
}
