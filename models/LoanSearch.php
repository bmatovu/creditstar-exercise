<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * LoanSearch represents the model behind the search form of `app\models\Loan`.
 */
class LoanSearch extends Loan
{
    /**
     * Loan: user's full name.
     *
     * @var string
     */
    public $user_name;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'duration', 'campaign'], 'integer'],
            [['amount', 'interest'], 'number'],
            [['user_name', 'start_date', 'end_date'], 'safe'],
            [['status'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied.
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Loan::find();
        $query->joinWith(['user']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'user_name' => [
                    'asc' => ['user.first_name' => SORT_ASC, 'user.last_name' => SORT_ASC],
                    'desc' => ['user.first_name' => SORT_DESC, 'user.last_name' => SORT_DESC],
                    'label' => 'User',
                    'default' => SORT_ASC,
                ],
                'amount',
                'interest',
                'duration',
                'start_date',
                'end_date',
                'campaign',
                'status',
            ],
        ]);

        $this->load($params);

        if (! $this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'amount' => $this->amount,
            'interest' => $this->interest,
            'duration' => $this->duration,
            // 'start_date' => $this->start_date,
            // 'end_date' => $this->end_date,
            'campaign' => $this->campaign,
            'status' => $this->status,
        ]);

        $query->andWhere("start_date::TEXT ILIKE '%{$this->start_date}%'");

        $query->andWhere("end_date::TEXT ILIKE '%{$this->end_date}%'");

        // $query->andWhere("start_date > '%{$this->start_date}%'");

        // $query->andWhere("end_date < '%{$this->end_date}%'");

        $query->andWhere("\"user\".first_name ILIKE '%{$this->user_name}%' OR \"user\".last_name ILIKE '%{$this->user_name}%'");

        return $dataProvider;
    }
}
