<?php

namespace app\models;

/**
 * This is the model class for table "loan".
 *
 * @property int $id
 * @property int $user_id
 * @property float $amount
 * @property float $interest
 * @property int $duration
 * @property string $start_date
 * @property string $end_date
 * @property int $campaign
 * @property bool|null $status
 */
class Loan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'loan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'amount', 'interest', 'duration', 'start_date', 'end_date', 'campaign'], 'required'],
            [['user_id', 'duration', 'campaign'], 'default', 'value' => null],
            [['user_id'], 'integer'],
            [['duration', 'campaign'], 'integer', 'max' => 30],
            [['amount'], 'number', 'max' => 10000000],
            [['interest'], 'number', 'max' => 100],
            [['start_date', 'end_date'], 'date', 'format' => 'yyyy-mm-dd'],
            ['end_date', 'checkAfterStartDate'],
            [['status'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User',
            'amount' => 'Amount (€)',
            'interest' => 'Interest (%)',
            'duration' => 'Duration (Days)',
            'start_date' => 'Starts',
            'end_date' => 'Ends',
            'campaign' => 'Campaign',
            'status' => 'Status',
        ];
    }

    /**
     * User that took this loan.
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), [
            'id' => 'user_id',
        ]);
    }

    /**
     * Validate end date must be after start date.
     */
    public function checkAfterStartDate()
    {
        $end_date = new \Datetime($this->end_date);

        $start_date = new \Datetime($this->start_date);

        if (! $this->hasErrors() && $end_date < $start_date) {
            $this->addError('end_date', 'End date must be after start date.');
        }
    }
}
