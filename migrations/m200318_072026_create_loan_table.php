<?php

use yii\db\Migration;

/**
 * Class m200318_072026_create_loan_table.
 */
class m200318_072026_create_loan_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%loan}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->bigInteger()->notNull(),
            'amount' => $this->decimal(10, 2)->notNull(),
            'interest' => $this->decimal(5, 2)->notNull(),
            'duration' => $this->integer()->notNull(),
            'start_date' => $this->date()->notNull(),
            'end_date' => $this->date()->notNull(),
            'campaign' => $this->integer()->notNull(),
            'status' => $this->boolean(),
        ]);

        $this->addForeignKey('fk_loan_user_id_user_id', '{{%loan}}', 'user_id', '{{%user}}', 'id', 'restrict', 'cascade');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_loan_user_id_user_id', '{{%loan}}');

        $this->dropTable('{{%loan}}');
    }
}
