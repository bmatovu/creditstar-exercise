<?php

use yii\db\Migration;

/**
 * Class m200318_072025_create_user_table.
 */
class m200318_072025_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'first_name' => $this->string(50)->notNull(),
            'last_name' => $this->string(50)->notNull(),
            'email' => $this->string(100)->notNull(),
            'personal_code' => $this->bigInteger()->notNull(),
            'phone' => $this->bigInteger()->notNull(),
            'active' => $this->boolean(1),
            'dead' => $this->boolean(0),
            'lang' => $this->string(5),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user}}');
    }
}
