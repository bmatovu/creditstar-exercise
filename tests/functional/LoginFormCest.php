<?php
class LoginFormCest
{
    public function _before(\FunctionalTester $I)
    {
        $I->amOnRoute('site/login');
    }

    public function openLoginPage(\FunctionalTester $I)
    {
        $I->see('Login', 'button');
    }

    // // demonstrates `amLoggedInAs` method
    // public function internalLoginById(\FunctionalTester $I)
    // {
    //     $I->amLoggedInAs(1);
    //     $I->amOnPage('/');
    //     $I->see('Logout (jdoe)');
    // }

    // demonstrates `amLoggedInAs` method
    public function internalLoginByInstance(\FunctionalTester $I)
    {
        $I->amLoggedInAs(\app\models\Staff::findByEmail('jdoe@example.com'));
        $I->amOnPage('/');
        $I->see('Logout', 'a');
    }

    public function loginWithEmptyCredentials(\FunctionalTester $I)
    {
        $I->submitForm('#login-form', []);
        $I->expectTo('see validations errors');
        $I->see('Email cannot be blank.');
        $I->see('Password cannot be blank.');
    }

    public function loginWithWrongCredentials(\FunctionalTester $I)
    {
        $I->submitForm('#login-form', [
            'LoginForm[email]' => 'jdoe@example.com',
            'LoginForm[password]' => 'wrong',
        ]);
        $I->expectTo('see validations errors');
        $I->see('Incorrect email or password.');
    }

    public function loginSuccessfully(\FunctionalTester $I)
    {
        $I->submitForm('#login-form', [
            'LoginForm[email]' => 'jdoe@example.com',
            'LoginForm[password]' => '12345678',
        ]);
        $I->see('Logout');
        $I->dontSeeElement('form#login-form');
    }
}
