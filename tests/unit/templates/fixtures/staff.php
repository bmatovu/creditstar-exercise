<?php

return [
    'alias' => $faker->name,
    'name' => $faker->name,
    'email' => $faker->unique()->safeEmail,
    'password' => Yii::$app->getSecurity()->generatePasswordHash('12345678'),
    'auth_key' => Yii::$app->getSecurity()->generateRandomString(),
    'access_token' => null,
    'created_at' => date('Y-m-d H:i:s'),
    'deleted_at' => $faker->boolean,
];
