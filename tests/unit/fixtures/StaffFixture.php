<?php

namespace app\tests\unit\fixtures;

use yii\test\ActiveFixture;

class StaffFixture extends ActiveFixture
{
    public $modelClass = 'app\models\Staff';
    public $dataFile = '@app/tests/unit/fixtures/data/staff.php';
}
