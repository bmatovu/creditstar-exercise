<?php

namespace app\tests\unit\fixtures;

use yii\test\ActiveFixture;

class LoanFixture extends ActiveFixture
{
    public $modelClass = 'app\models\Loan';
    public $depends = ['app\tests\unit\fixtures\UserFixture'];
    public $dataFile = '@app/tests/unit/fixtures/data/loans.php';
}
