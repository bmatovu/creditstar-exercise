<?php

return [
    [
        'alias' => 'jdoe',
        'name' => 'John Doe',
        'email' => 'jdoe@example.com',
        'password' => '$2y$13$3eJ6Sk6Zdwcrmqs7Pi27N.A3ESdPFcWCfdxtdlWYThms9WIVIMJcC', // 12345678
        'auth_key' => 'jdoe-key',
        'access_token' => 'jdoe-token',
        'created_at' => '2020-03-18 11:33:52',
        'deleted_at' => false,
    ],
];
