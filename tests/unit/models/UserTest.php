<?php
namespace tests\models;

use app\models\User;

class UserTest extends \Codeception\Test\Unit
{
    public function testDeterminesCorrectAge()
    {
        $user = new User();
        $user->first_name = 'John';
        $user->last_name = 'Doe';
        $user->email = 'jdoe@example.com';
        $user->personal_code = 39105280193;
        $user->phone = 257985654258;
        $user->active = true;
        $user->dead = false;
        $user->lang = 'est';
        $user->save();

        $dob = new \DateTime('1991-05-28');

        $this->assertEquals($dob->format('Y-m-d'), $user->birthDate->format('Y-m-d'));

        $now = new \DateTime();

        $this->assertEquals($now->diff($dob)->y, $user->age);

        $user->delete();
    }
}
