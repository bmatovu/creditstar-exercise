<?php
namespace tests\models;

use app\models\Staff;

class StaffTest extends \Codeception\Test\Unit
{
    public function testFindStaffById()
    {
        expect_that($staff = Staff::findIdentity(1));
        expect($staff->email)->equals('jdoe@example.com');

        expect_not(Staff::findIdentity(999));
    }

    public function testFindStaffByAccessToken()
    {
        expect_that($staff = Staff::findIdentityByAccessToken('jdoe-token'));
        expect($staff->email)->equals('jdoe@example.com');

        expect_not(Staff::findIdentityByAccessToken('non-existing'));
    }

    public function testFindStaffByEmail()
    {
        expect_that($staff = Staff::findByEmail('jdoe@example.com'));
        expect_not(Staff::findByEmail('unknown@example.com'));
    }

    /**
     * @depends testFindStaffByEmail
     *
     * @param mixed $staff
     */
    public function testValidateStaff($staff)
    {
        $staff = Staff::findByEmail('jdoe@example.com');
        expect_that($staff->validateAuthKey('jdoe-key'));
        expect_not($staff->validateAuthKey('wrong-key'));

        expect_that($staff->validatePassword('12345678'));
        expect_not($staff->validatePassword('wrong-password'));
    }
}
