<?php

// @var $this yii\web\View

use yii\helpers\Html;

$this->title = Yii::$app->name;
?>

<div class="flex-center position-ref full-height">
    <div class="top-right links">
        <?php if (Yii::$app->staff->isGuest): ?>
            <a href="/site/login">Login</a>
        <?php else: ?>
            <a href="/site/logout" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                Logout
            </a>
            <?= Html::beginForm(['/site/logout'], 'post', ['id' => 'logout-form']) ?>
            <?= Html::endForm() ?>
        <?php endif; ?>
    </div>

    <div class="content">
        <div class="title m-b-md">
            <?= Yii::$app->name ?>
        </div>

        <?php if (Yii::$app->staff->isGuest): ?>
            <div class="">
                <p>See test user (email | password) below</p>
                <p><code>jdoe@example.com</code> | <code>12345678</code></p>
            </div>
        <?php else: ?>
            <div class="links">
                <a href="/user/index">Users</a>
                <a href="/loan/index">Loans</a>
            </div>
        <?php endif; ?>
    </div>
</div>
