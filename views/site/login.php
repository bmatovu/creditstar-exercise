<?php

// @var $form yii\bootstrap\ActiveForm
// @var $model app\models\LoginForm

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Login';
?>

<div class="row row-centered min-w-100">

    <div class="col-md-4 col-sm-6 col-xs-12 col-centered">

        <div class="brand-name">
            <?= Html::a(Yii::$app->name, ['site/index'], ['class' => '']) ?>
        </div>

        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
        ]) ?>

            <?= $form->field($model, 'email')->input('email', ['autofocus' => true]) ?>

            <?= $form->field($model, 'password')->passwordInput() ?>

            <?= $form->field($model, 'rememberMe')->checkbox() ?>

            <?= Html::submitButton('Login', ['class' => 'btn btn-block btn-custom btn-round', 'name' => 'login-btn']) ?>

        <?php ActiveForm::end(); ?>

    </div>

</div>
