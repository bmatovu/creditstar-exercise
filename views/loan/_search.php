<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

// @var $this yii\web\View
// @var $model app\models\LoanSearch
// @var $form yii\widgets\ActiveForm
?>

<div class="row loan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

        <div class="col-md-3 col-sm-4 col-xs-6">
            <?= $form->field($model, 'user_name')->label('User') ?>
        </div>

        <div class="col-md-3 col-sm-4 col-xs-6">
            <?= $form->field($model, 'amount')->textInput(['type' => 'number']) ?>
        </div>

        <div class="col-md-3 col-sm-4 col-xs-6">
            <?= $form->field($model, 'interest')->textInput(['type' => 'number']) ?>
        </div>

        <div class="col-md-3 col-sm-4 col-xs-6">
            <?= $form->field($model, 'start_date')
                ->label('Starts')
                ->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => 'Enter start date ...'],
                    'pluginOptions' => [
                        'autoclose'=> true,
                        'format' => 'yyyy-mm-dd',
                    ],
                ]) ?>
        </div>

        <div class="col-md-3 col-sm-4 col-xs-6">
            <?= $form->field($model, 'end_date')
                ->label('Ends')
                ->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => 'Enter end date ...'],
                    'pluginOptions' => [
                        'autoclose'=> true,
                        'format' => 'yyyy-mm-dd',
                    ],
                ]);
            ?>
        </div>

        <div class="form-group col-md-3 col-sm-4 col-xs-6">
            <label class="control-label" for="">&nbsp;</label>
            <?= Html::submitButton('Search', ['class' => 'btn btn-block btn-primary']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>
