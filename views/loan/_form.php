<?php

use app\models\User;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

// @var $this yii\web\View
// @var $model app\models\Loan
// @var $form yii\widgets\ActiveForm
?>

<div class="row loan-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">

        <?=
            $form->field($model, 'user_id')
                ->dropDownList(ArrayHelper::map(User::find()->all(), 'id', 'name'), ['prompt' => 'Choose user...'])
                ->label('User')
        ?>

        <?= $form->field($model, 'amount')->input('number')->label('Amount (€)') ?>

        <?= $form->field($model, 'interest')->input('number')->label('Interest (%)') ?>

        <?= $form->field($model, 'duration')->input('number')->label('Duration (Days)') ?>

        </div>

        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">

        <?=
            $form->field($model, 'start_date')
                ->label('Starts')
                ->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => 'Enter start date ...'],
                    'pluginOptions' => [
                        'autoclose'=> true,
                        'format' => 'yyyy-mm-dd',
                        'startDate'=> '0d',
                        'todayHighlight' => true,
                    ],
                ])
        ?>

        <?=
            $form->field($model, 'end_date')
                ->label('Ends')
                ->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Enter end date ...'],
                'pluginOptions' => [
                    'autoclose'=> true,
                    'format' => 'yyyy-mm-dd',
                ],
            ]);
        ?>

        <?= $form->field($model, 'campaign')->input('number') ?>

        <?= $form->field($model, 'status')->checkbox() ?>

        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-block btn-primary']) ?>
        </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>
