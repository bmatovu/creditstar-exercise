<?php

use yii\helpers\Html;

// @var $this yii\web\View
// @var $model app\models\Loan

$this->title = "Loans / {$model->id} / Update";
?>
<div class="loan-update">

    <div class="row">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/site/index">Home</a></li>
                <li class="breadcrumb-item"><a href="/loan/index">Loans</a></li>
                <li class="breadcrumb-item"><?= Html::a($model->id, ['view', 'id' => $model->id]) ?></li>
                <li class="breadcrumb-item active" aria-current="page">Edit</li>
            </ol>
        </nav>
    </div>

    <div class="row">
        <?php if (yii::$app->session->hasFlash('message')): ?>
            <div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <?php echo yii::$app->session->getFlash('message'); ?>
            </div>
        <?php endif ?>
    </div>

    <?= $this->render('_form', ['model' => $model]) ?>

</div>
