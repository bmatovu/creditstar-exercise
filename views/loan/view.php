<?php

use yii\helpers\Html;

// @var $this yii\web\View
// @var $model app\models\User

$this->title = "Loans / {$model->id}";
\yii\web\YiiAsset::register($this);
?>
<div class="loan-view">

    <div class="row">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/site/index">Home</a></li>
                <li class="breadcrumb-item"><a href="/loan/index">Loans</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?= $model->id ?></li>
            </ol>
        </nav>
    </div>

    <div class="row">
        <?php if (yii::$app->session->hasFlash('message')): ?>
            <div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <?php echo yii::$app->session->getFlash('message'); ?>
            </div>
        <?php endif ?>
    </div>

    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?= Html::encode($model->id) ?>
                <div class="pull-right">
                    <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-info btn-xs']) ?>
                    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger btn-xs',
                        'data' => [
                            'confirm' => "Are you sure you want to delete {$model->id}?",
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>
            </div>

            <div class="panel-body">

                <div class="row vrow">
                    <label class="control-label vlabel col-lg-3 col-md-4 col-sm-5 col-xs-12">
                        User
                    </label>
                    <label class="control-label vvalue col-lg-9 col-md-8 col-sm-7 col-xs-12">
                        <?= $model->user->name ?>
                    </label>
                </div>

                <div class="row vrow">
                    <label class="control-label vlabel col-lg-3 col-md-4 col-sm-5 col-xs-12">
                        Amount (€)
                    </label>
                    <label class="control-label vvalue col-lg-9 col-md-8 col-sm-7 col-xs-12">
                        <?= $model->amount ?>
                    </label>
                </div>

                <div class="row vrow">
                    <label class="control-label vlabel col-lg-3 col-md-4 col-sm-5 col-xs-12">
                        Interest (%)
                    </label>
                    <label class="control-label vvalue col-lg-9 col-md-8 col-sm-7 col-xs-12">
                        <?= $model->interest ?>
                    </label>
                </div>

                <div class="row vrow">
                    <label class="control-label vlabel col-lg-3 col-md-4 col-sm-5 col-xs-12">
                        Duration (days)
                    </label>
                    <label class="control-label vvalue col-lg-9 col-md-8 col-sm-7 col-xs-12">
                        <?= $model->duration ?>
                    </label>
                </div>

                <div class="row vrow">
                    <label class="control-label vlabel col-lg-3 col-md-4 col-sm-5 col-xs-12">
                        Starts
                    </label>
                    <label class="control-label vvalue col-lg-9 col-md-8 col-sm-7 col-xs-12">
                        <?= (new \DateTime($model->start_date))->format('D, jS M Y') ?>
                    </label>
                </div>

                <div class="row vrow">
                    <label class="control-label vlabel col-lg-3 col-md-4 col-sm-5 col-xs-12">
                        Ends
                    </label>
                    <label class="control-label vvalue col-lg-9 col-md-8 col-sm-7 col-xs-12">
                        <?= (new \DateTime($model->end_date))->format('D, jS M Y') ?>
                    </label>
                </div>

                <div class="row vrow">
                    <label class="control-label vlabel col-lg-3 col-md-4 col-sm-5 col-xs-12">
                        Campaign
                    </label>
                    <label class="control-label vvalue col-lg-9 col-md-8 col-sm-7 col-xs-12">
                        <?= $model->campaign ?>
                    </label>
                </div>

                <div class="row vrow">
                    <label class="control-label vlabel col-lg-3 col-md-4 col-sm-5 col-xs-12">
                        Status
                    </label>
                    <label class="control-label vvalue col-lg-9 col-md-8 col-sm-7 col-xs-12">
                        <?= $model->status ?
                                Html::label('Open', 'status', ['class' => 'label label-sm label-success']) :
                                Html::label('Closed', 'status', ['class' => 'label label-sm label-danger']);
                        ?>
                    </label>
                </div>

            </div>

        </div>
    </div>

</div>
