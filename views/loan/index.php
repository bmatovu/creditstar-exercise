<?php

use yii\grid\GridView;
use yii\helpers\Html;

// @var $this yii\web\View
// @var $searchModel app\models\LoanSearch
// @var $dataProvider yii\data\ActiveDataProvider

$this->title = 'Loans';
?>
<div class="loan-index">

    <div class="row">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/site/index">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Loans</li>
            </ol>
        </nav>
    </div>

    <div class="row">
        <?php if (yii::$app->session->hasFlash('message')): ?>
            <div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <?php echo yii::$app->session->getFlash('message'); ?>
            </div>
        <?php endif ?>
    </div>

    <?= $this->render('_search', ['model' => $searchModel]); ?>

    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?= Html::encode($this->title) ?>
                <div class="pull-right">
                    <a href="create" class="btn btn-default btn-xs">
                        <span class="glyphicon glyphicon-plus"></span> Register
                    </a>
                </div>
            </div>
            <div class="panel-body row">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'layout' => "<div class='col-xs-12'><div class='table-responsive'>{items}</div>\n{summary}\n<div class='text-center'>{pager}</div></div>",
                    'tableOptions' => [
                        'class' => 'table table-striped table-hover text-nowrap',
                    ],
                    'columns' => [
                        [
                            'attribute' => 'user_name',
                            'label' => 'User',
                            'format' => 'raw',
                            'value' => function ($loan) {
                                return Html::a(
                                    $loan->user->name,
                                    [
                                        'user/view',
                                        'id' => $loan->user->id,
                                    ],
                                    [
                                        'title' => 'View User',
                                        'class' => '',
                                    ]
                                );
                            },
                        ],
                        [
                            'attribute' => 'amount',
                            'contentOptions' => ['class' => 'text-right'],
                            'headerOptions' => ['class' => 'text-right'],
                        ],
                        [
                            'attribute' => 'interest',
                            'contentOptions' => ['class' => 'text-right'],
                            'headerOptions' => ['class' => 'text-right'],
                        ],
                        [
                            'attribute' => 'duration',
                            'contentOptions' => ['class' => 'text-right'],
                            'headerOptions' => ['class' => 'text-right'],
                        ],
                        [
                            'attribute' => 'start_date',
                            'contentOptions' => ['class' => 'text-center'],
                            'headerOptions' => ['class' => 'text-center'],
                        ],
                        [
                            'attribute' => 'end_date',
                            'contentOptions' => ['class' => 'text-center'],
                            'headerOptions' => ['class' => 'text-center'],
                        ],
                        [
                            'attribute' => 'campaign',
                            'contentOptions' => ['class' => 'text-right'],
                            'headerOptions' => ['class' => 'text-right'],
                        ],
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'contentOptions' => ['class' => 'text-center'],
                            'headerOptions' => ['class' => 'text-center'],
                            'value' => function ($loan) {
                                return $loan->status ?
                                    Html::label('Open', 'status', ['class' => 'label label-sm label-success']) :
                                    Html::label('Closed', 'status', ['class' => 'label label-sm label-danger']);
                            },
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'contentOptions' => ['class' => 'actions'],
                            'template' => '{view} {update}',
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>
