<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

// @var $this yii\web\View
// @var $model app\models\User
$this->title = 'Users / Register';
?>
<div class="user-create">

    <div class="row">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/site/index">Home</a></li>
                <li class="breadcrumb-item"><a href="/user/index">Users</a></li>
                <li class="breadcrumb-item active" aria-current="page">Register</li>
            </ol>
        </nav>
    </div>

    <div class="row">
        <?php if (yii::$app->session->hasFlash('message')): ?>
            <div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <?php echo yii::$app->session->getFlash('message'); ?>
            </div>
        <?php endif ?>
    </div>

    <div class="row" style="background-color: #f5f5f5; padding-top: 20px; padding-bottom: 5px; margin-bottom: 20px; border-radius: 5px;">

        <?php $form = ActiveForm::begin([
            'action' => ['create'],
            'method' => 'post',
        ]); ?>

        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">

            <?= $form->field($model, 'first_name')->textInput() ?>

            <?= $form->field($model, 'last_name')->textInput() ?>

            <?= $form->field($model, 'email')->input('email') ?>

            <?= $form->field($model, 'personal_code')->input('number') ?>

        </div>

        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">

            <?= $form->field($model, 'phone')->input('tel') ?>

            <?= $form->field($model, 'active')->checkbox() ?>

            <?= $form->field($model, 'dead')->checkbox() ?>

            <?= $form->field($model, 'lang')->dropDownList(['est' => 'Estonian', 'en' => 'English']) ?>

            <div class="form-group ">
                <?= Html::submitButton('Save', ['class' => 'btn btn-block btn-primary']) ?>
            </div>

        </div>

        <?php ActiveForm::end(); ?>

    </div>

    <?php // $this->render('_form', ['model' => $model])?>

</div>
