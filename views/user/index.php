<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

// @var $this yii\web\View
// @var $searchModel app\models\UserSearch
// @var $dataProvider yii\data\ActiveDataProvider

$this->title = 'Users';
?>
<div class="user-index">

    <div class="row">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/site/index">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Users</li>
            </ol>
        </nav>
    </div>

    <div class="row">
        <?php if (yii::$app->session->hasFlash('message')): ?>
            <div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <?php echo yii::$app->session->getFlash('message'); ?>
            </div>
        <?php endif ?>
    </div>

    <div class="row" style="background-color: #f5f5f5; padding-top: 20px; padding-bottom: 5px; margin-bottom: 20px; border-radius: 5px;">

        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]); ?>

            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6">
                <?= $form->field($searchModel, 'name') ?>
            </div>

            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6">
                <?= $form->field($searchModel, 'email') ?>
            </div>

            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6">
                <?= $form->field($searchModel, 'personal_code')->textInput(['type' => 'number']) ?>
            </div>

            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6">
                <?= $form->field($searchModel, 'phone')->textInput(['type' => 'number']) ?>
            </div>

            <div class="form-group col-lg-2 col-md-3 col-sm-4 col-xs-6">
                <label class="control-label" for="">&nbsp;</label>
                <?= Html::submitButton('Search', ['class' => 'btn btn-block btn-primary']) ?>
            </div>

        <?php ActiveForm::end(); ?>

    </div>

    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?= Html::encode($this->title) ?>
                <div class="pull-right">
                    <a href="create" class="btn btn-default btn-xs">
                        <span class="glyphicon glyphicon-plus"></span> Register
                    </a>
                </div>
            </div>
            <div class="panel-body row">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'layout' => "<div class='col-xs-12'><div class='table-responsive'>{items}</div>\n{summary}\n<div class='text-center'>{pager}</div></div>",
                    'tableOptions' => [
                        'class' => 'table table-striped table-hover text-nowrap',
                    ],
                    'columns' => [
                        [
                            'attribute' => 'name',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return Html::a($model->name, ['user/view', 'id' => $model->id]);
                            },
                        ],
                        [
                            'attribute' => 'email',
                            'format' => 'email',
                            'contentOptions' => ['class' => 'text-left'],
                            'headerOptions' => ['class' => 'text-left'],
                        ],
                        [
                            'attribute' => 'personal_code',
                            'contentOptions' => ['class' => 'text-right'],
                            'headerOptions' => ['class' => 'text-right'],
                        ],
                        [
                            'attribute' => 'phone',
                            'label' => 'Phone No.',
                            'contentOptions' => ['class' => 'text-right'],
                            'headerOptions' => ['class' => 'text-right'],
                        ],
                        [
                            'attribute' => 'active',
                            'format' => 'raw',
                            'contentOptions' => ['class' => 'text-center'],
                            'headerOptions' => ['class' => 'text-center'],
                            'value' => function ($user) {
                                return $user->active ?
                                    Html::label('Yes', 'status', ['class' => 'label label-sm label-success']) :
                                    Html::label('No', 'status', ['class' => 'label label-sm label-danger']);
                            },
                        ],
                        [
                            'attribute' => 'dead',
                            'format' => 'raw',
                            'contentOptions' => ['class' => 'text-center'],
                            'headerOptions' => ['class' => 'text-center'],
                            'value' => function ($user) {
                                return $user->dead ?
                                    Html::label('Yes', 'status', ['class' => 'label label-sm label-danger']) :
                                    Html::label('No', 'status', ['class' => 'label label-sm label-success']);
                            },
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'contentOptions' => ['class' => 'actions'],
                            'buttons' => [
                                'loans' => function ($url, $model, $key) {
                                    $query = http_build_query(['LoanSearch' => ['user_id' => $model->id]]);

                                    return Html::a(
                                        '<span class="glyphicon glyphicon-th-list" aria-hidden="true"></span> ',
                                        ["loan/index?{$query}"]
                                    );
                                },
                            ],
                            'template' => '{update} {loans}',
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>

</div>
