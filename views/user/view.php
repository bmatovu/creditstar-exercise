<?php

use yii\helpers\Html;

// @var $this yii\web\View
// @var $model app\models\User

$this->title = "Users / {$model->name}";
\yii\web\YiiAsset::register($this);
?>
<div class="user-view">

    <div class="row">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/site/index">Home</a></li>
                <li class="breadcrumb-item"><a href="/user/index">Users</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?= $model->name ?></li>
            </ol>
        </nav>
    </div>

    <div class="row">
        <?php if (yii::$app->session->hasFlash('message')): ?>
            <div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <?php echo yii::$app->session->getFlash('message'); ?>
            </div>
        <?php endif ?>
    </div>

    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?= Html::encode($model->name) ?>
                <div class="pull-right">
                    <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-info btn-xs']) ?>
                    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger btn-xs',
                        'data' => [
                            'confirm' => "Are you sure you want to delete {$model->name}?",
                            'method' => 'post',
                        ],
                    ]) ?>
                </div>
            </div>

            <div class="panel-body">

                <div class="row vrow">
                    <label class="control-label vlabel col-lg-3 col-md-4 col-sm-5 col-xs-12">
                        Name
                    </label>
                    <label class="control-label vvalue col-lg-9 col-md-8 col-sm-7 col-xs-12">
                        <?= $model->name ?>
                    </label>
                </div>

                <div class="row vrow">
                    <label class="control-label vlabel col-lg-3 col-md-4 col-sm-5 col-xs-12">
                        Date of birth
                    </label>
                    <label class="control-label vvalue col-lg-9 col-md-8 col-sm-7 col-xs-12">
                        <?= "{$model->birthDate->format('D, jS M Y')} (Age: {$model->age})" ?>
                    </label>
                </div>

                <div class="row vrow">
                    <label class="control-label vlabel col-lg-3 col-md-4 col-sm-5 col-xs-12">
                        Email Address
                    </label>
                    <label class="control-label vvalue col-lg-9 col-md-8 col-sm-7 col-xs-12">
                        <?= Html::mailto($model->email) ?>
                    </label>
                </div>

                <div class="row vrow">
                    <label class="control-label vlabel col-lg-3 col-md-4 col-sm-5 col-xs-12">
                        Personal Code
                    </label>
                    <label class="control-label vvalue col-lg-9 col-md-8 col-sm-7 col-xs-12">
                        <?= $model->personal_code ?>
                    </label>
                </div>

                <div class="row vrow">
                    <label class="control-label vlabel col-lg-3 col-md-4 col-sm-5 col-xs-12">
                        Phone No.
                    </label>
                    <label class="control-label vvalue col-lg-9 col-md-8 col-sm-7 col-xs-12">
                        <?= $model->phone ?>
                    </label>
                </div>

                <div class="row vrow">
                    <label class="control-label vlabel col-lg-3 col-md-4 col-sm-5 col-xs-12">
                        Is Active?
                    </label>
                    <label class="control-label vvalue col-lg-9 col-md-8 col-sm-7 col-xs-12">
                        <?= $model->active ? 'Yes' : 'No' ?>
                    </label>
                </div>

                <div class="row vrow">
                    <label class="control-label vlabel col-lg-3 col-md-4 col-sm-5 col-xs-12">
                        Is Dead?
                    </label>
                    <label class="control-label vvalue col-lg-9 col-md-8 col-sm-7 col-xs-12">
                        <?= $model->dead ? 'Yes' : 'No' ?>
                    </label>
                </div>

                <div class="row vrow">
                    <label class="control-label vlabel col-lg-3 col-md-4 col-sm-5 col-xs-12">
                        Language
                    </label>
                    <label class="control-label vvalue col-lg-9 col-md-8 col-sm-7 col-xs-12">
                        <?= $model->lang ?>
                    </label>
                </div>

            </div>

        </div>
    </div>

</div>
