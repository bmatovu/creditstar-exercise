<?php

// @var $this \yii\web\View
// @var $content string

use app\assets\AppAsset;
use yii\helpers\Html;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700&display=swap" rel="stylesheet">
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

    <header>
        <nav class="navbar navbar-default navbar-fixed-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="/site/index">Loaner</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="/user/index">Users</a></li>
                    <li><a href="/loan/index">Loans</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <strong><?= Yii::$app->staff->identity->alias ?></strong><span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                          <li>
                                <?php if (! Yii::$app->staff->isGuest): ?>
                                    <a href="/site/logout" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>
                                    <?= Html::beginForm(['/site/logout'], 'post', ['id' => 'logout-form']) ?>
                                    <?= Html::endForm() ?>
                                <?php endif; ?>
                          </li>
                        </ul>
                    </li>
                </ul>
            </div>
          </div>
        </nav>
    </header>

    <main class="container">
        <?= $content ?>
    </main>

    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <p>&copy; <?= Yii::$app->name ?> <?= date('Y') ?>. All rights reserved.</p>
                </div>
            </div>
        </div>
    </footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
