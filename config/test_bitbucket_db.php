<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'pgsql:host=localhost;port=5432;dbname=pipelines',
    'username' => 'test_user',
    'password' => 'test_user_password',
    'charset' => 'utf8',
];
