<?php

$driver = env('TEST_DB_DRIVER', 'pgsql');
$host = env('TEST_DB_HOST', '127.0.0.1');
$port = env('TEST_DB_PORT', 5432);
$database = env('TEST_DB_DATABASE');

return [
    'class' => \yii\db\Connection::class,
    'dsn' => "{$driver}:host={$host};port={$port};dbname={$database}",
    'username' => env('TEST_DB_USERNAME'),
    'password' => env('TEST_DB_PASSWORD'),
    'charset' => 'utf8',
];
