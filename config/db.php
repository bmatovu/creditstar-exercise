<?php

$driver = env('DB_DRIVER', 'pgsql');
$host = env('DB_HOST', '127.0.0.1');
$port = env('DB_PORT', 5432);
$database = env('DB_DATABASE');

return [
    'class' => \yii\db\Connection::class,
    'dsn' => "{$driver}:host={$host};port={$port};dbname={$database}",
    'username' => env('DB_USERNAME'),
    'password' => env('DB_PASSWORD'),
    'charset' => 'utf8',
];
